var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('styles', function() {
    return gulp.src('src/sass/main.scss').pipe(sass()).pipe(gulp.dest('css'))
});

gulp.task('js', function() {
    return gulp.src('src/js/*.js').pipe(gulp.dest('js'))
});

gulp.task('watch', function () {
    gulp.watch(['src/sass/*.scss', 'src/js/*.js'], ['styles', 'js']);
});
