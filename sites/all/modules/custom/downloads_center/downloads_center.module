<?php

function downloads_center_menu() {

  $items = array();

  $items['admin/settings/downloads-center'] = array(
    'title' => 'Downloads center',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer nodes'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/settings/downloads-center/regions'] = array(
    'title' => 'Regions',
    'description' => 'Choose region for language.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('downloads_center_regions_page'),
    'access arguments' => array('administer site configuration'),
  );

  $items['admin/settings/downloads-center/departments'] = array(
    'title' => 'Departments',
    'description' => 'Choose department for product.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('downloads_center_departments_page'),
    'access arguments' => array('administer site configuration'),
  );

  $items['admin/settings/downloads-center/request-form'] = array(
    'title' => 'Request form',
    'description' => 'Choose request form.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('downloads_center_choose_request_form_page'),
    'access arguments' => array('administer site configuration'),
  );

  $items['admin/settings/downloads-center/request-form-submissions'] = array(
    'title' => 'Request form submissions',
    'description' => 'Request form submissions.',
    'page callback' => 'downloads_center_request_form_submissions_page',
    'access arguments' => array('administer site configuration'),
  );

  $items['admin/settings/downloads-center/header-need-help'] = array(
    'title' => 'Header Need Help widget',
    'description' => 'Header Need Help widget configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' =>  array('header_need_help_form_page'),
    'access arguments' => array('administer site configuration'),
  );

  return $items;
}

function downloads_center_departments_page() {
  $form = array();
  $departments = [];
  // Get products list
  $vocabulary = taxonomy_vocabulary_machine_name_load('widen_product');
  $parent_terms = taxonomy_get_tree($vocabulary->vid, 0, 1);
  foreach ( $parent_terms as $term ) {
    $products[$term->tid] = $term->name;
  }
  // Get departments list
  $query = db_select('node', 'n')
    ->fields('n', array('nid', 'title'))
    ->condition('type', 'department', '=')
    ->orderBy('n.title', 'ASC');

  $result = $query->execute()->fetchAll();

  foreach ($result as $department) {
    $departments[$department->nid] = $department->title;
  }

  $form['default_department'] = array(
    '#type' => 'select',
    '#title' => t('Default department'),
    '#default_value' => variable_get('default_department'),
    '#description' => t('Select department'),
    '#size' => 1,
    '#required' => TRUE,
    '#options' => $departments
  );

  foreach ($departments as $id => $department){
    $form['department_settings_' . $id] = [
      '#type' => 'fieldset',
      '#title' => t('@department', array('@department' => $department)),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['department_settings_' . $id]['products_for_department_' . $id] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 5,
      '#title' => t('Products'),
      '#default_value' => variable_get('products_for_department_' . $id),
      '#description' => t('Select products'),
      '#required' => FALSE,
      '#options' => $products
    );
  }

  // extra
  $form['suffix'] = array(
    '#type' => 'markup',
    '#markup' => '<script type="text/javascript">
    (function($){
      $(document).ready(function(){
        $("#block-system-main select").each(function() {
          $(this).multiSelect({keepOrder:true});
        });
      });
    })(jQuery);
    </script>',
  );
  $library = libraries_get_path('multi-select');
  $form['#attached'] = array(
    'css' => array(
      $library . '/css/multi-select.css',
    ),
    'js' => array(
      $library . '/js/jquery.multi-select.js',
    ),
  );

  return system_settings_form($form);
}

function downloads_center_regions_page() {
  $form = array();
  // Get regions list
  $vocabulary = taxonomy_vocabulary_machine_name_load('widen_country');
  $parent_terms = taxonomy_get_tree($vocabulary->vid, 0, 1);
  $regions = [];
  foreach ( $parent_terms as $term ) {
    $regions[$term->tid] = $term->name;
  }
  // Get document languages list
  $vocabulary = taxonomy_vocabulary_machine_name_load('widen_language');
  $parent_terms = taxonomy_get_tree($vocabulary->vid, 0, 1);
  $widen_languages = [];
  foreach ( $parent_terms as $term ) {
    $widen_languages[$term->tid] = $term->name;
  }

  $languages_list = i18n_language_list();

  foreach ($languages_list as $key => $language) {
    $form['language_settings_' . $key] = [
      '#type' => 'fieldset',
      '#title' => t('@language', array('@language' => $language)),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['language_settings_' . $key]['column_left'] = [
      '#prefix' => '<div class="column">',
      '#suffix' => '</div>',
    ];

    $form['language_settings_' . $key]['column_left']['region_for_' . $key] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 5,
      '#title' => t('Region'),
      '#default_value' => variable_get('region_for_' . $key),
      '#description' => t('Select region'),
      '#required' => TRUE,
      '#options' => $regions
    );

    $form['language_settings_' . $key]['column_right'] = [
      '#prefix' => '<div class="column">',
      '#suffix' => '</div>',
    ];

    $form['language_settings_' . $key]['column_right']['language_for_' . $key] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 5,
      '#title' => t('Language'),
      '#default_value' => variable_get('language_for_' . $key),
      '#description' => t('Select default language for region'),
      '#required' => TRUE,
      '#options' => $widen_languages
    );
  }

  // extra
  $form['suffix'] = array(
    '#type' => 'markup',
    '#markup' => '<script type="text/javascript">
    (function($){
      $(document).ready(function(){
        $("#block-system-main select").each(function() {
          $(this).multiSelect({keepOrder:true});
        });
      });
    })(jQuery);
    </script>',
  );
  $library = libraries_get_path('multi-select');
  $form['#attached'] = array(
    'css' => array(
      $library . '/css/multi-select.css',
    ),
    'js' => array(
      $library . '/js/jquery.multi-select.js',
    ),
  );

  return system_settings_form($form);
}

function downloads_center_choose_request_form_page() {
  $form = [];
  $options = [];

  $query = db_select('node', 'n')
    ->fields('n', array('nid', 'title'))
    ->condition('type', 'webform', '=');

  $result = $query->execute()->fetchAll();

  foreach ($result as $webform) {
    $options[$webform->nid] = $webform->title;
  }

  $form['request_form'] = [
    '#type' => 'select',
    '#title' => t('Choose form'),
    '#default_value' => variable_get('request_form'),
    '#required' => TRUE,
    '#options' => $options
  ];

  return system_settings_form($form);
}

function downloads_center_request_form_submissions_page() {
  $view = views_get_view('webform_submissions');
  $view->set_display('default');
  $view->set_arguments([variable_get('request_form')]);
  $view->pre_execute();
  $view->execute();
  // Return the rendered View.
  return $view->render();
}

function header_need_help_form_page() {
  $languages = language_list();

  foreach ($languages as $language) {
    if ($language->enabled) {

      $form['language_' . $language->language] = [
        '#type' => 'fieldset',
        '#title' => check_plain($language->name),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      ];

      $form['language_' . $language->language]['header_help_title_' . $language->language] = [
        '#title' => 'Help Title',
        '#type' => 'textfield',
        '#default_value' => variable_get('header_help_title_' . $language->language, 'Need Help'),
      ];

      $form['language_' . $language->language]['header_help_text_' . $language->language] = [
        '#title' => 'Help Text',
        '#type' => 'textfield',
        '#default_value' => variable_get('header_help_text_' . $language->language, 'Call 800.537.7123'),
      ];
    }
  }

  return system_settings_form($form);
}

function downloads_center_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#id'] == 'views-exposed-form-downloads-center-page') {
    global $language;

    $form['all_languages'] = [
      '#type' => 'checkbox',
      '#title' => t('<span>Can not find IFU?</span> Display IFUs in all available languages'),
      '#attributes' => [
        'onload' => 'showInAllLanguages(this);',
        'onchange' => 'showInAllLanguages(this);',
      ]
    ];

    $region_languages = variable_get('language_for_' . $language->language);
    $regions = variable_get('region_for_' . $language->language);

    if (isset($form_state['input']['all_languages']) && $form_state['input']['all_languages'] == 1){
      $form_state['input']['field_resource_language_tid'] = [];
    } else {
      $form_state['input']['field_resource_language_tid'] = $region_languages;
      $form['field_resource_language_tid']['#default_value'] = $region_languages;
    }

    $form_state['input']['field_resource_country_tid'] = $regions;
    $form['field_resource_country_tid']['#default_value'] = $regions;

    $languages_list = [];
    foreach ( $region_languages as $lid => $region_language ) {
      $languages_list[] = $lid;
    }
    $language_filter = implode(',', $languages_list);

    $regions_list = [];
    foreach ( $regions as $rid => $region ) {
      $regions_list[] = $rid;
    }
    $region_filter = implode(',', $regions_list);

    $products_view = views_get_view('downloads_center_products_filter_');
    $products_view->set_display('block');
    $products_view->set_arguments([$language_filter,$region_filter]);
    $products_view->execute();
    $results = $products_view->result;

    $products_list = ['All' => t('- Any -')];
    $part_numbers_list = ['All' => t('- Any -')];
    foreach ($results as $result) {
      if ( !empty($result->field_field_resource_product) ) {
        $products_list[$result->field_field_resource_product[0]['raw']['tid']] = $result->field_field_resource_product[0]['raw']['taxonomy_term']->name;
      }

      if (!empty($result->field_field_resource_part_number)) {
        $part_numbers_list[$result->field_field_resource_part_number[0]['raw']['tid']] = $result->field_field_resource_part_number[0]['raw']['taxonomy_term']->name;
      }
    }

    $form['field_resource_part_number_tid']['#options'] = $part_numbers_list;

    $form['field_resource_product_tid']['#options'] = $products_list;

    $submit = $form['submit'];
    unset($form['submit']);
    $form['submit'] = $submit;

    $script = '<script>
            function showInAllLanguages(el) {
                var defaultLanguage = jQuery(\'#edit-field-widen-languages-value\').val();

                if (jQuery(el).prop(\'checked\')){
                    jQuery(\'#edit-field-widen-languages-value\').data(\'data-default-language\', defaultLanguage).val(\' \').change();
                } else {
                    defaultLanguage = jQuery(\'#edit-field-widen-languages-value\').data(\'data-default-language\');
                    jQuery(\'#edit-field-widen-languages-value\').val(defaultLanguage).change();
                }
            }

            jQuery(document).ready(function ($) {
                var language = jQuery(\'#edit-field-widen-languages-value\').val();
                if (language == \'All\'){
                    jQuery(\'#edit-all-languages\').prop(\'checked\', true);
                }
            });
        </script>';

    $form['script'] = [
      '#markup' => $script
    ];
  }

  if ($form_id == 'views_ui_config_item_form' && !empty($form['options']['content'])) {
    // Views edit forms: Enable CKEditor
    $form['options']['content']['#wysiwyg'] = TRUE;
  }
}

function downloads_center_preprocess_html(&$variables) {
// Add conditional stylesheets for admin pages on admin theme.
  if (arg(0) == "admin") {
    // reference your own stylesheet
    drupal_add_css(drupal_get_path('module', 'downloads_center') . '/css/admin.css', array('weight' => CSS_SYSTEM));
  }
}

function downloads_center_webform_submission_insert($node, $submission) {
  $request_form = variable_get('request_form');
  if ($request_form == $node->nid) {
    $components = $node->webform['components'];

    $widen_asset = NULL;
    foreach ($components as $id => $component) {
      if ($component['form_key'] == 'widen_asset') {
        $widen_asset = node_load($submission->data[$id][0]);
      }
    }

    if ($widen_asset) {
      foreach ($components as $id => $component) {
        $field_data = field_get_items('node', $widen_asset, $component['form_key'], LANGUAGE_NONE);
        switch ($component['form_key']) {
          case 'field_resource_widen_updated':
            $date = date('D, M d, Y', $field_data[0]['value']);
            $submission->data[$id][0] = $date;
            break;
          case 'field_resource_country':
          case 'field_resource_language':
          case 'field_resource_product':
            $value = '';
            $count = count($field_data);
            if ($field_data && is_array($field_data)) {
              foreach ($field_data as $key => $item) {
                $taxonomy_term = taxonomy_term_load($item['tid']);
                if ($key < $count - 1) {
                  $value .= $taxonomy_term->name . ', ';
                }
                else {
                  $value .= $taxonomy_term->name;
                }
              }
            }
            $submission->data[$id][0] = $value;
            break;
          case 'field_project_job_number':
          case 'field_resource_download_url':
          case 'field_resource_file_size':
          case 'field_resource_uuid':
          case 'field_asset_title':
          case 'field_resource_file_name':
            $value = $field_data[0]['value'];
            $submission->data[$id][0] = $value;
            break;
        }
      }

      $products = field_get_items('node', $widen_asset, 'field_resource_product', LANGUAGE_NONE);
      $emails = [];

      if ($products) {
        // Get departments list
        $query = db_select('node', 'n')
          ->fields('n', array('nid', 'title'))
          ->condition('type', 'department', '=');

        $result = $query->execute()->fetchAll();
        $departments = [];
        foreach ($result as $department) {
          $departments[$department->nid] = variable_get('products_for_department_' . $department->nid);
        }

        foreach ($products as $product) {
          $departments_emails = _get_departments_emails_by_product_name($product['tid'], $node, $departments);
          $emails = array_merge($emails, $departments_emails);
        }
      }
      else {
        $emails[] = _get_departments_emails_by_product_name(NULL, $node);
      }

      webform_submission_update($node, $submission);
      webform_submission_send_mail($node, $submission, $emails);
    }
  }
}

function _get_departments_emails_by_product_name($product_tid, $webform, $departments = []) {
  $product_departments = [];
  foreach ($departments as $id => $products){
    if ( isset($products[$product_tid]) ) {
      $product_departments[] = $id;
    }
  }

  if (empty($product_departments)){
    $product_departments[] = variable_get('default_department');
  }

  $emails = [];

  foreach ($product_departments as $product_department){
    $department = node_load($product_department);
    $department_email = field_get_items('node', $department, 'field_department_email');

    $email = [
      'nid' => $webform->nid,
      'email' => $department_email[0]['email'],
      'subject' => 'default',
      'from_name' => 'default',
      'from_address' => 'default',
      'template' => 'default',
      'html' => 1,
      'attachments' => 0,
      'status' => 1,
    ];

    $emails[] = $email;
  }

  return $emails;
}

/**
 * Break x,y,z and x+y+z into an array. Works for strings.
 *
 * @param $str
 *   The string to parse.
 * @param $object
 *   The object to use as a base. If not specified one will
 *   be created.
 *
 * @return $object
 *   An object containing
 *   - operator: Either 'and' or 'or'
 *   - value: An array of numeric values.
 */
function downloads_center_views_break_phrase_string($str, &$handler = NULL) {
  if (!$handler) {
    $handler = new stdClass();
  }

  // Set up defaults:
  if (!isset($handler->value)) {
    $handler->value = array();
  }

  if (!isset($handler->operator)) {
    $handler->operator = 'or';
  }

  if ($str == '') {
    return $handler;
  }

  $or_wildcard = '[^\s+,]';
  $and_wildcard = '[^+,]';
  if (preg_match("/^({$or_wildcard}+[+ ])+{$or_wildcard}+$/", $str)) {
    $handler->operator = 'or';
    $handler->value = preg_split('/[+]/', $str);
  }
  elseif (preg_match("/^({$and_wildcard}+,)*{$and_wildcard}+$/", $str)) {
    $handler->operator = 'and';
    $handler->value = explode(',', $str);
  }

  // Keep an 'error' value if invalid strings were given.
  if (!empty($str) && (empty($handler->value) || !is_array($handler->value))) {
    $handler->value = array(-1);
    return $handler;
  }

  // Doubly ensure that all values are strings only.
  foreach ($handler->value as $id => $value) {
    $handler->value[$id] = (string) $value;
  }

  return $handler;
}

/**
 * Implements hook_views_api().
 */
function downloads_center_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implements hook_views_data_alter().
 */
function downloads_center_views_data_alter(&$data) {
//  $data['field_data_field_widen_languages']['field_widen_languages_value']['argument']['handler'] = 'downloads_center_views_handler_argument_field_list_string';
//  $data['field_data_field_widen_country']['field_widen_country_value']['argument']['handler'] = 'downloads_center_views_handler_argument_field_list_string';
}

function downloads_center_form_views_exposed_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#id'] == 'views-exposed-form-downloads-center-page') {
    foreach($form_state['view']->filter as $filter) {
      if($filter->options['expose']['identifier'] == 'field_resource_product_tid')
        natcasesort($form['field_resource_product_tid']['#options']);
    }
  }
}
