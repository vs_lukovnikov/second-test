(function ($) {
  window.widendam_galleryEmbedCallback = function (uuid, msg) {
    if (msg.indexOf('href') > 1) {
      var code = removeSizeTags(msg);
      editor.insertHtml(code);
      overlay = jQuery('#widendam-overlay');
      container = jQuery('#widendam-container');
      iframe = jQuery('#widendam-iframe');

      iframe.hide();
      container.hide();
      overlay.hide();
    }
    else {
      var src_end = msg.split('src="');
      var src_end2 = src_end[1].split('"');
      var url = src_end2[0];
      var node_path = window.location.pathname.replace('/node/', '').split('/');
      var frameUrl = new URL(window.location.href);
      var CKEditorFuncNum = frameUrl.searchParams.get("CKEditorFuncNum");
      jQuery.ajax({
        url: Drupal.settings.basePath + 'ajax/kerrdental-widen/save-asset',
        type: 'post',
        data: 'nodeid=' + node_path[0] + '&uuid=' + encodeURIComponent(uuid) + '&url=' + encodeURIComponent(url) + '&CKEditorFuncNum=' + CKEditorFuncNum,
        success: function (r) {
          if (r.status == 'success') {
            window.o.CKEDITOR.tools.callFunction(r.funcNum, r.fileUrl);
            window.close();
          }
          else if (r.status == 'error') {
            alert(r.message);
          }
        }
      });
    }
  }
  $(document).ready(function () {

    $('#toolbar').hide();
    // $('#branding').hide();

    $('#widen_search_button').bind('click', function () {
      // Reset to page 1 for new searches
      $('#widen_pg').val(1);
    });

    $('select').bind('change', function () {
      var select_id = $(this)[0].id;
      var id_array = select_id.split('_');
      var uuid = id_array[id_array.length - 1];
      var code_id = '#widencode_' + uuid;

      $(code_id).val(this.value);
    });

    $('.widen_embed_link').bind('click', function () {
      var embed_id = $(this)[0].id;
      var id_array = embed_id.split('_');
      var uuid = id_array[id_array.length - 1];
      var code_id = '#widencode_' + uuid;
      var code = $(code_id)[0].value;

      if ((window.location.pathname).split('/')[4] == 'gallery') {
        var par = window.parent,
            op = window.opener,
            o = (par && par.CKEDITOR) ? par : ((op && op.CKEDITOR) ? op : false);
        if (o !== false) {
          window.o = o;
          window.parent.widendam_galleryEmbedCallback(uuid, code);
        }
        else {
          if (op) {
            window.close();
          }
        }
      }
      else {
        if ($('body').hasClass('page-media-browser')) {
          window.parent.kerrdental_widen_embedRef(uuid, code);
          return false;
        }
        window.parent.widendam_embedCallback(uuid, code);
      }
    });

    $('#widen_first').bind('click', function () {
      $('#widen_pg').val(Drupal.settings.widendam.first);
      $("#widendam-search-form").submit();
    });

    $('#widen_prev').bind('click', function () {
      $('#widen_pg').val(Drupal.settings.widendam.prev);
      $("#widendam-search-form").submit();
    });

    $('#widen_next').bind('click', function () {
      $('#widen_pg').val(Drupal.settings.widendam.next);
      $("#widendam-search-form").submit();
    });

    $('#widen_last').bind('click', function () {
      $('#widen_pg').val(Drupal.settings.widendam.last);
      $("#widendam-search-form").submit();
    });

    $('#widendam-search-form select').each(function () {
      $(this).select2();
    });

  });

})(jQuery);
