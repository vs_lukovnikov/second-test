<?php

function widendam_search($parm = 'blank')
{
    $build = array();
    $content = array();

    if (!widendam_is_configured())
    {
        $config_msg = "The Widen DAM module must be configured by an Administrator before searching Widen DAM content.";
        drupal_set_message($config_msg, 'error');
        return $build;
    }
    if (!widendam_has_access_token())
    {
        $auth_msg = "You must authorize with Widen DAM on the config page";
        drupal_set_message($auth_msg, 'error');
        return $build;
    }
    
    $params = drupal_get_query_parameters();

    $current_page = 1;
    if (isset($_POST['pg']))
    {
        $current_page = $_POST['pg'];
    }

    $term = null;
    if (isset($_POST['search']))
    {
        $term = $_POST['search'];
    }

    $content['search_part'] = drupal_get_form('widendam_search_form', $term, $current_page, $parm, $params);
    $search_blank = true;
    
    if (isset($_POST['pg']))
    {
        $post = drupal_get_query_parameters($_POST);
        if ($term) {
          $term = urlencode($term);
          $term = str_replace('+', '%20', $term);
        }
        
        $search_blank = false;
        $max = widendam_get_pagesize($parm);
        $start = ($current_page - 1) * widendam_get_pagesize($parm);
        
        $filters = $filters_save = array();
        // CONSTANT FILTERS
        $filters['rd'] = '[before ' . date("m/d/Y", strtotime("+1 day")) . ']'; // release date - add one day to get before tomorrow (today)
        $filters['ed'] = '[after ' . date("m/d/Y") . '] OR ed:isempty'; // expiration date
        // PRE-FILTERS
        $prefilters = kerrdental_widen_prefilters_array();
        foreach ($prefilters as $prefilter) {
          if ($filter_terms = variable_get('kerrdental_widen_prefilters_images_' . $prefilter['key'])) {
            $type_filters = array();
            $terms = taxonomy_term_load_multiple($filter_terms);
            foreach ($terms as $t) {
              $type_filters[] = '{'. $t->name . '}';
            }
            $filters[$prefilter['key']] = implode(' or ', $type_filters);
          }  
        }
        // FILTERS
        // file format (sent in popup url - set in drupal field settings)
        if (isset($params['file_extensions']) && $params['file_extensions']) {
          $filters['ff'] = str_replace(' ', ' or ', $params['file_extensions']);
          // if jpg is in filters, add other image-like formats (will be converted to acceptable format on download)
          if (strpos($filters['ff'], 'jpg') !== FALSE) {
            $filters['ff'] .= ' or eps or psd or tif';
          }
        }
        //$image_filters = array('fn', 'pn', 'cat', 'pro');
        if (isset($post['fn']) && $post['fn']) {
          $filters['fn'] = $post['fn'];
          $filters_save['fn'] = $post['fn'];
        }
        if (isset($post['pn']) && $post['pn']) {
          $filters['pn'] = $post['pn'];
          $filters_save['pn'] = $post['pn'];
        }
        if (isset($post['cat'])) {
          $filters_save['cat'] = $post['cat'];
          $type_filters = array();
          $terms = taxonomy_term_load_multiple($post['cat']);
          foreach ($terms as $t) {
            $type_filters[] = '{'. $t->name . '}';
          }
          $filters['cat'] = implode(' or ', $type_filters);
          
        }
        if (isset($post['pro'])) {
          $filters_save['pro'] = $post['pro'];
          $type_filters = array();
          $terms = taxonomy_term_load_multiple($post['pro']);
          foreach ($terms as $t) {
            $type_filters[] = '{'. $t->name . '}';
          }
          $filters['pro'] = implode(' or ', $type_filters);
        }
        drupal_add_js(array('widendam_filters' => json_encode($filters_save)), 'setting'); // js will save back to parent node
        $result = widendam_api_v2_search_by_expression($term, $start, $max, true, $filters, FALSE);
        $content['results_part'] = drupal_get_form('widendam_results_form', $term, $result, $current_page, $parm);
    }

    drupal_add_js(array('widendam' => array('searchBlank' => $search_blank)), 'setting');
    $build['content'] = $content;

    return $build;
}

function widendam_search_form($form, &$form_state, $term, $current_page, $parm, $params = array())
{
  $vocab_cat = taxonomy_vocabulary_machine_name_load('widen_categories');
  $vocab_pr = taxonomy_vocabulary_machine_name_load('widen_product');

    /*$form['search'] = array(
        '#type' => 'textfield',
        '#default_value' => $term,
        '#prefix' => '<div class="widen_search_box">',
        '#suffix' => '</div>',
        '#attributes' => array(
          'placeholder' => 'Search term',
        ),
    );*/
    $class = '';
    if (isset($_POST['pg'])) {
      $class = 'process-results';
    }
    $form['prefix'] = array(
      '#type' => 'markup',
      '#markup' => '<div id="widen-search-boxer" class="' . $class . '">',
    );
    $form['fn'] = array(
        '#type' => 'textfield',
        '#title' => 'Widen  File Name',
        '#size' => 30,
        '#prefix' => '<div>',
    );
    $form['pn'] = array(
      '#type' => 'textfield',
      '#title' => 'Widen Part #',
      '#size' => 30,
    );
    $form['cat'] = array(
      '#type' => 'select',
      '#title' => 'Widen Categories',
      '#multiple' => TRUE,
      '#options' => kerrdental_widen_taxonomy_to_select_options($vocab_cat->vid), // widen categories
      '#prefix' => '</div><div>',
      '#attributes' => array(
         'style' => 'width: 220px',
      ),
    );
    $form['pro'] = array(
      '#type' => 'select',
      '#title' => 'Widen Products',
      '#multiple' => TRUE,
      '#options' => kerrdental_widen_taxonomy_to_select_options($vocab_pr->vid), // widen products
      '#attributes' => array(
         'style' => 'width: 220px',
      ),
    );  

    // "Hidden" form field modified by javascript to drive paging
    $form['pg'] = array(
        '#type' => 'textfield',
        '#id' => 'widen_pg',
        '#default_value' => $current_page,
        '#prefix' => '<div style="display:none;">',
        '#suffix' => '</div>',
    );

    $form['search_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Search',
        '#id' => 'widen_search_button',
        '#submit' => array('widendam_search_submit')

    );
    $form['suffix'] = array(
      '#type' => 'markup',
      '#markup' => '</div></div><a href="#" id="widen-search-box-show" class="' . $class . '">Show search form</a>',
    );

    $path = drupal_get_path('module', 'widendam');
    $theme = drupal_get_path('theme', 'kerrdental');
    $form['#attached'] = array
    (
        'css' => array
        (
            $theme . '/css/select2.min.css',
            $path . '/css/widendam_search.css',
        ),
        'js' => array
        (
            $theme . '/js/select2.min.js',
            $path . '/js/widendam_search.js',
        ),
    );
    return $form;
}

function widendam_search_submit($form, &$form_state)
{
    $form_state['rebuild'] = TRUE;
}

function widendam_results_form($form, &$form_state, $term, $result, $current_page, $parm)
{
    $numResults = $result->total_count;
    $assets = $result->items;

    if (isset($_POST['pg']))
    {

        /*if ($numResults > 0)
        {
            $selectAll = '<a href="#" id="widen_select_all">all</a>';
            $selectNone = '<a href="#" id="widen_select_none">none</a>';
            $form['batchSelect'] = array(
                '#type' => 'markup',
                '#markup' => '<br/><b>Select: </b>' . $selectAll . ", " . $selectNone// . '<br/>',
            );
        }*/

        $widen_page_nav = '';
        if ($numResults > widendam_get_pagesize($parm))
        {
            $total_pages = ceil($numResults / widendam_get_pagesize($parm));
            $next_page = $current_page + 1;
            $previous_page = $current_page - 1;

            if ($next_page < 1) { $next_page = 1; }
            if ($next_page > $total_pages) { $next_page = $total_pages; }

            $asset_range_low = 1 + (($current_page - 1) * widendam_get_pagesize($parm));
            $asset_range_high = $asset_range_low + widendam_get_pagesize($parm) - 1;
            if ($asset_range_high > $numResults) { $asset_range_high = $numResults; }

            $prev_enabled = $current_page > 1;
            $next_enabled = $current_page < $total_pages;

            drupal_add_js(array('widendam' => array('first' => 1)), 'setting');
            drupal_add_js(array('widendam' => array('prev' => ($current_page - 1))), 'setting');
            drupal_add_js(array('widendam' => array('next' => ($current_page + 1))), 'setting');
            drupal_add_js(array('widendam' => array('last' => ($total_pages))), 'setting');

            $result_set = '<i>Displaying ' . $asset_range_low . ' - ' . $asset_range_high . '</i>  ';

            if ($prev_enabled)
            {
                $prev_links = ' <a id="widen_first" href="#"> &lt&lt </a> <a id="widen_prev" href="#"> &lt </a> ';
            }
            else
            {
                $prev_links = ' &lt&lt  &lt ';
            }

            if ($next_enabled)
            {
                $next_links = ' <a id="widen_next" href="#"> &gt </a> <a id="widen_last" href="#"> &gt&gt </a> ';
            }
            else
            {
                $next_links = ' &gt &gt&gt ';
            }

            $widen_page_nav = $result_set . $prev_links . ' Page ' . $current_page . ' of ' . $total_pages . ' ' . $next_links;
        }

        $form['nav'] = array(
            '#type' => 'markup',
            '#markup' => '<div class="widen_page_nav">' . $widen_page_nav . '</div>',
        );
        
        $form['numResults'] = array(
            '#type' => 'markup',
            '#markup' => '<h3>' . $numResults . ' results found</h3>', //for \'' . urldecode($term) . '\'</h3>',
        );

        foreach ($assets as $asset)
        {
            $filename = $asset->filename;
            $uuid = $asset->id;
            $fileType = $asset->file_properties->format_type;
            $thumbnail = str_replace(['{size}', '{scale}', '{quality}'], ['125', '2', '100'], $asset->embeds->templated->url);
            $detailsLink = url('admin/content/widen/details/' . $uuid);
            $popupJs = 'window.open(\'' . $detailsLink . '\', \'\', \'width=800, height=800, resizable=1\'); return false;';

            $prefix = '<div class="widen_search_result ' . widendam_get_results_css_class($parm) . '">';
            $prefix .= '<a href="#" onclick = "' . $popupJs . '">';
            $prefix .= '<img id="widen_thumbnail_' . $uuid . '" class="widen_thumbnail" src="' . $thumbnail . '" alt="Image Not Available" />';

            $prefix .= '</a>';
            $prefix .= '<div class="widen_filename">';

            $embed_codes = array();
            $embeds = (array) $asset->embeds;
            unset($embeds['original']);
            unset($embeds['templated']);
            foreach($embeds as $key => $image) {

              $embed_codes[$image->html] = $key;
            }

            $form['code_' . $uuid] = array(
                '#type' => 'textfield',
                '#id' => 'widencode_' . $uuid,
                '#default_value' => key($embed_codes),  // First key
                '#prefix' => '<div style="display:none;">',
                '#suffix' => '</div>',
            );

            $form['filename_' . $uuid] = array(
                '#type' => 'select',
                '#id' => 'widenfilename_' . $uuid,
                '#prefix' => $prefix . $filename . '<br/>[' . $fileType . ']',
                '#options' => $embed_codes,
                '#suffix' => '<a href="javascript:void(0)" id="widenembed_' . $uuid . '" class="widen_embed_link"><button type="button">Embed</button></a></div></div>',
            );
        }

        $path = drupal_get_path('module', 'widendam');
        $form['#attached'] = array
        (
            'css' => array
            (
                'type' => 'file',
                'data' => $path . '/css/widendam_results.css',
            ),
            /*'js' => array
            (
                'type' => 'file',
                'data' => $path . '/js/widendam_results.js',
            ),*/
        );

        return $form;
    }
}

function widendam_get_pagesize($parm)
{
    return WIDEN_PAGE_SIZE_DEFAULT;
}

function widendam_get_results_css_class($parm)
{
    return "widen_results_embed";
}
